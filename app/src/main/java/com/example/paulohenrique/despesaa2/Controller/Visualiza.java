package com.example.paulohenrique.despesaa2.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.example.paulohenrique.despesaa2.Banco.BancoDados;
import com.example.paulohenrique.despesaa2.Descricao;
import com.example.paulohenrique.despesaa2.DespesaAdapter;
import com.example.paulohenrique.despesaa2.R;
import com.example.paulohenrique.despesaa2.Tipos.Despesa;

import java.util.ArrayList;

public class Visualiza extends AppCompatActivity {
    //Codigo da tela
    final int cod = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualiza);

        //Cria listview, lvdespesa esta no xml Visualiza
        final ListView listView = (ListView) findViewById(R.id.lvdespesa);

//Cria instancia do banco de dados, passando nome da tabela que será manipula durante a execução
        BancoDados classBD = new BancoDados(getBaseContext(), "despesa", 1);

//Atributo do tipo Array, passando as referencia de despesa.
        //Recebe os valores do metodo de busca do banco
        ArrayList<Despesa> listarray = classBD.listaDespesa();

        BaseAdapter baseAdapter = new DespesaAdapter(listarray, this);

        //Recebe o base adapter da classe despesa adapter
        listView.setAdapter(baseAdapter);
        //evento para abertura do item selecionado do listview
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //Recebe o item do listview, onde passa todos os valores
                Despesa des = (Despesa) listView.getAdapter().getItem(i);

                //responsavel por enviar os dados para outra tela
                Bundle dados = new Bundle();
                //os valores id, observao são as chaves para utilizar na proxima tela
                dados.putString("id", String.valueOf(des.getId()));
                dados.putString("observacao", String.valueOf(des.getDescricao()));
                dados.putString("data", String.valueOf(des.getData()));
                dados.putString("valor", String.valueOf(des.getValor()));

                //Responsavel por abrir a tela de Descrição
                Intent intent = new Intent(Visualiza.this, Descricao.class);
                //envia os dados para a proxima tela
                intent.putExtras(dados);
                //finaliza a tela na pilha
                finish();
                //ação para abrir a nova tela
                startActivityForResult(intent, cod);
            }
        });
    }
}