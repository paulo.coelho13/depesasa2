package com.example.paulohenrique.despesaa2.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.paulohenrique.despesaa2.R;

public class Principal extends AppCompatActivity {

    Button btadd;
    Button btlistar;
    Button bttotal;
    Button sair;
    Intent intent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        btadd = (Button) findViewById(R.id.btadd);
        btlistar = (Button) findViewById(R.id.btlistar);
        bttotal = (Button) findViewById(R.id.bttotal);
        sair = (Button) findViewById(R.id.sair);

        //Ação do botão adicionar novo usuario
        btadd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //chama o metodo de abrir nova tela
                abrirAdicionar();
            }
        });

        //Ação do botão adicionar novo usuario
        bttotal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //chama o metodo de abrir total gasto
                abrirTotal();
            }
        });

        //Ação do botão para sair do login do usuario
        sair.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //chama o metodo de sair
                sair();
            }
        });
    }

    public void abrirAdicionar() {
        intent = new Intent(this, CadastrarDespesa.class);
        startActivity(intent);
    }

    public void abrirListar(View v) {
        intent = new Intent(this, Visualiza.class);
        startActivity(intent);
    }

    public void abrirTotal() {
        intent = new Intent(this, DespesaTotal.class);
        startActivity(intent);
    }

    public void sair() {
        intent = new Intent(this, Login.class);
        finish();
        startActivity(intent);
    }

}