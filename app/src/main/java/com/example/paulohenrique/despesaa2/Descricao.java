package com.example.paulohenrique.despesaa2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.paulohenrique.despesaa2.Banco.BancoDados;
import com.example.paulohenrique.despesaa2.Controller.Principal;
import com.example.paulohenrique.despesaa2.Controller.Visualiza;
import com.example.paulohenrique.despesaa2.Tipos.Despesa;

public class Descricao extends AppCompatActivity {

    //Atributos
    TextView idlbl;
    EditText obslbl;
    EditText dalalbl;
    EditText valorlbl;
    Button excluirdes;
    Button atualidesp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descricao);

        //Trecho responsavel por receber os valores do listview
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        //atribui aos atributos os valores de cada chave
        String ida = bundle.getString("id");
        String observacao = bundle.getString("observacao");
        String data = bundle.getString("data");
        String valor = bundle.getString("valor");

        idlbl = (TextView) findViewById(R.id.idlabel);
        obslbl = (EditText) findViewById(R.id.observacaolabel);
        dalalbl = (EditText) findViewById(R.id.dalalabel);
        dalalbl.addTextChangedListener(Mask.insert("##/##/####", dalalbl));
        valorlbl = (EditText) findViewById(R.id.valorlabel);

        idlbl.setText(ida);
        obslbl.setText(observacao);
        dalalbl.setText(data);
        valorlbl.setText(valor);

        excluirdes = (Button) findViewById(R.id.excluirdes);
        atualidesp = (Button) findViewById(R.id.atualidesp);

        //Evento do botão de atualizawr
        atualidesp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //parte responsavel por passar os valores inseridos para os atributos da classe
                //atribuito iram ser enviados para a classe despesa
                int id = Integer.valueOf(idlbl.getText().toString());
                String obs = obslbl.getText().toString();
                String data = dalalbl.getText().toString();
                String valor = valorlbl.getText().toString();

                //cria instancia da classe do banco de dados
                BancoDados bds = new BancoDados(getBaseContext(), "despesa", 1);
                //Cria instancia da classe despesa, onde irão ser passados os valores necessários para a classe
                Despesa des = new Despesa();
                des.setId(id);
                des.setDescricao(obs);
                des.setData(data);
                des.setValor(valor);

                try {
                    //chamada o metodo da classe do banco, passando o objeto do tipo despesa
                    bds.atualizar(des);
                    //mensagem que será enviada para tela caso seja inserida corretamente
                    Toast.makeText(getApplicationContext(), "Despesa atualizada com sucesso!", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    //mensagem que será enviada para tela caso não atualize
                    Toast.makeText(getApplicationContext(), "Não foi possivel atualizar!", Toast.LENGTH_LONG).show();
                }
                //trecho responsavel para chamar nova tela, Visualiza.class é a classe que sera chamada
                Intent intent = new Intent(getApplicationContext(), Visualiza.class);
                //fecha a tela atual, evitando ficar na pilha
                finish();
                //ação para abrir a nova tela
                startActivity(intent);
            }
        });

        //Evento do botão de excluir
        excluirdes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //cria instancia da classe do banco de dados
                BancoDados bds = new BancoDados(getBaseContext(), "despesa", 1);
                //Cria instancia da classe despesa, onde irão ser passados os valores necessários para excluir a despesa
                Despesa des = new Despesa();
                //atributo recebe o valor do id da despesa, a conversão é necessária pois o valor é String
                int id = Integer.valueOf(idlbl.getText().toString());
                //seta o id da descrição para a classe despesa
                des.setId(id);
                //chama o metodo de excluir passando o objeto do tipo despesa
                bds.deletaItem(des);
                //trecho responsavel para chamar nova tela, Principal.class é a classe que sera chamada
                Intent intent = new Intent(getApplicationContext(), Principal.class);
                //fecha a tela atual, evitando ficar na pilha
                finish();
                //ação para abrir a nova tela
                startActivity(intent);
            }
        });
    }
}
