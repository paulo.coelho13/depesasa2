package com.example.paulohenrique.despesaa2.Controller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.paulohenrique.despesaa2.Banco.BancoDados;
import com.example.paulohenrique.despesaa2.R;

public class Login extends AppCompatActivity {
    Button btentrar;
    Button btcadastrar;
    EditText edilogin;
    EditText edisenha;
    BancoDados bds;

    CheckBox salvasenha;

    Intent intent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btentrar = (Button) findViewById(R.id.btentrar);
        btcadastrar = (Button) findViewById(R.id.btcadastrar);
        edilogin = (EditText) findViewById(R.id.edilogin);
        edisenha = (EditText) findViewById(R.id.edisenha);
        salvasenha = (CheckBox) findViewById(R.id.salvasenha);
        salvasenha.setChecked(true);

        //chama o metodo, caso tenha algum valor guardado é setado nos campos
        salvarsenha();

        //ação do botão
        btentrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //atributos recebem os valores dos campos
                String logins = edilogin.getText().toString();
                String senhas = edisenha.getText().toString();
                //atributo ira receber retorno do metodo do banco, verificando se existe ou não
                String dados;
                //instancia da classe do banco, passando o banco que sera usado
                bds = new BancoDados(getBaseContext(), "usuario", 1);

                //verifica se os campos foram preenchidos
                if(edilogin.getText().length() <= 0 || edisenha.getText().length() <= 0){
                    //caso não exista o usuario recebe uma mensagem informando
                    Toast.makeText(getApplicationContext(), "Preencha os campos corretamente", Toast.LENGTH_SHORT).show();
                } else {
                    //atributo recebe retorno do banco
                    //chama o metodo do banco passando o login e senha digitado pelo usuario
                    dados = bds.validaUsuario(logins, senhas);

                    //se o retorno recebe ok então é feito a abertura da tela
                    if(dados == "OK"){
                        //armazena o usuario no shared, caso o usuario selecione que deseja lembrar senha
                        SharedPreferences sharedPref = getSharedPreferences("preferencia",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        //verifica se a opção de salvar foi selecionada
                        if(salvasenha.isChecked()){
                            //grava os dados inseridos no sharedpref
                            editor.putString("email", edilogin.getText().toString());
                            editor.putString("senha", edisenha.getText().toString());
                            //ação de salvar
                            editor.apply();
                            //caso o usuario não marque o valores são setados em branco
                        } else {
                            editor.putString("email", "");
                            editor.putString("senha", "");
                            editor.apply();
                        }

                        //chama metodo para abrir a tela principal
                        abrePrincipal();
                        //limpa os campos após abertura da tela
                        edilogin.setText("");
                        edisenha.setText("");
                    } else if(dados == "ERRO") {
                        //caso não seja o usuario é notificado
                        Toast.makeText(getApplicationContext(), "Usuário inválido!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        //ação do checkbox
        salvasenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salvarsenha();
            }
        });

    }

    //metodo responsavel por setar os valores nos campos
    //caso tenha algo é prenchido
    public void salvarsenha(){
        SharedPreferences prefs = getSharedPreferences("preferencia", Context.MODE_PRIVATE);
        edilogin.setText(prefs.getString("email", ""));
        edisenha.setText(prefs.getString("senha", ""));
    }

    //ação de abrir a tela principal
    //é feito o desimpilhamento da tela atual caso seja aberto
    public void abrePrincipal(){
        intent = new Intent(this, Principal.class);
        finish();
        startActivity(intent);
    }

    //ação para abrir tela de cadastro do usuário
    //é finalizado a tela atual
    public void abreCadastro(View view){
        intent = new Intent(this, Cadastro.class);
        finish();
        startActivity(intent);
    }
}