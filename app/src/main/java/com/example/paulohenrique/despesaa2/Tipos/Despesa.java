package com.example.paulohenrique.despesaa2.Tipos;

public class Despesa {

    //Classe modelo, onde a despesa recebe todos os atributos
    //atributos da classe
    private int id;
    private String descricao;
    private String valor;
    private String data;

    //contrutos vazio
    public Despesa() {

    }

    //construtor parametrizado
    public Despesa(int id, String descricao, String data, String valor) {
        this.id = id;
        this.descricao = descricao;
        this.data = data;
        this.valor = valor;
    }

    //getters e setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
