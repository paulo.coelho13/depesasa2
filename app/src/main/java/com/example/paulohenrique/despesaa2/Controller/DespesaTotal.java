package com.example.paulohenrique.despesaa2.Controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.paulohenrique.despesaa2.Banco.BancoDados;
import com.example.paulohenrique.despesaa2.R;

public class DespesaTotal extends AppCompatActivity {

    TextView valor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_despesa_total);

        valor = (TextView) findViewById(R.id.valoras);

        //cria instancia do classe do banco de dados, passando a tabela que sera manipulada
        BancoDados bds = new BancoDados(getBaseContext(), "despesa", 1);
        //o atributo recebe o retorno do metodo do banco de dados
        String valortela = bds.somando();
        if(valortela != "R$00,00") {
            //caso o retorno seja diferente é setado o valor das despesas
            valor.setText(("R$" + valortela));
        } else {
            //caso não seja retornado o valor total é apenas apresentado o valor de 0
            valor.setText(valortela);
        }
    }
}
