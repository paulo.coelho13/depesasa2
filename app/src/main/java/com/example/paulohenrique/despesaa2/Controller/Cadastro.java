package com.example.paulohenrique.despesaa2.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.paulohenrique.despesaa2.Banco.BancoDados;
import com.example.paulohenrique.despesaa2.R;

public class Cadastro extends AppCompatActivity {

    EditText editemail;
    EditText editsenha;
    EditText editnome;
    EditText confirmesenha;
    Button btsalvar;

    String senha, confirma;
    Context context;
    Intent intent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        editemail = (EditText) findViewById(R.id.editemail);
        editsenha = (EditText) findViewById(R.id.editsenha);
        editnome = (EditText) findViewById(R.id.editnome);
        confirmesenha = (EditText) findViewById(R.id.confirmesenha);
        btsalvar = (Button) findViewById(R.id.btsalvar);
    }

    public void salvarUsuario(View view) {
        //atribui os valores do campo para o atributos da classe
        senha = String.valueOf(editsenha.getText());
        confirma = String.valueOf(confirmesenha.getText());
        //verifica se os campos digitados estão vazios
        if (editemail.getText().length() == 0 || editnome.getText().length() == 0 || editsenha.getText().length() == 0) {
            //caso seja vazio o usuario visualiza uma mensagem informando para preencher
            Toast.makeText(getApplicationContext(), "Preencha os campos corretamente", Toast.LENGTH_SHORT).show();
        } else {
            //verifica se as duas senhas inseridas são iguais
            if (senha.equals(confirma)) {
                //contentvalues ira receber os valores do campo
                ContentValues contentValues = new ContentValues();
                //cada chave ira receber os valores correspondentes
                contentValues.put("nome", editnome.getText().toString());
                contentValues.put("email", editemail.getText().toString());
                contentValues.put("senha", editsenha.getText().toString());

                //Cria instancia da classe do banco de dados, passando o nome da tabela que ira ser manipulada
                BancoDados classBD = new BancoDados(getBaseContext(), "usuario", 1);
                //passa para os metodo de inserir o contentvalues com seus repectivos valores
                classBD.insereUsuario(contentValues);

                //abre uma nova tela, tela de login, apos o cadastro ser efetuado
                intent = new Intent(this, Login.class);
                //finaliza a tela de cadastro na pilha
                finish();
                //ação para abrir nova tela
                startActivity(intent);
            } else {
                //caso não seja o usuario é notificado
                Toast.makeText(getApplicationContext(), "Senhas não correspondem", Toast.LENGTH_SHORT).show();
            }
        }
    }
}