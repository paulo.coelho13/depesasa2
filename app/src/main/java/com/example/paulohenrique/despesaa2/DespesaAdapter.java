package com.example.paulohenrique.despesaa2;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.paulohenrique.despesaa2.Tipos.Despesa;

import java.util.ArrayList;

public class DespesaAdapter extends BaseAdapter {

    //Array de despesa
    ArrayList<Despesa> desp;
    private Activity activity;

    public DespesaAdapter(ArrayList<Despesa> desp, Activity activity) {
        this.desp = desp;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        //returna o tamanho do arraylist
        return desp.size();
    }

    @Override
    public Object getItem(int i) {
        return desp.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View reciclado, ViewGroup parent) {

        View layout = null;

        if (reciclado == null){
            //Layout recebe a celula para preenche o list view
            layout = LayoutInflater.from(activity).inflate(R.layout.celula, parent, false);
        }else{
            layout = reciclado;
        }

        //Instancia da classe despesa, passando o item
        final Despesa despesaa = (Despesa) getItem(i);

        //Cria os textview da celula
        TextView iddespesa = (TextView) layout.findViewById(R.id.lblid);
        TextView observacaodes = (TextView) layout.findViewById(R.id.lblobservacao);
        TextView dataobs = (TextView) layout.findViewById(R.id.lbldata);
        TextView valorobs = (TextView) layout.findViewById(R.id.lblvalor);

        //passa os valores da classe despesa para os textviews, onde sera apresentado no listview
        iddespesa.setText(String.valueOf (despesaa.getId()));
        observacaodes.setText(String.valueOf(despesaa.getDescricao()));
        valorobs.setText(String.valueOf(despesaa.getValor()));
        dataobs.setText(String.valueOf(despesaa.getData()));

        //retorna o layout com os valores preenchidos
        return layout;
    }
}