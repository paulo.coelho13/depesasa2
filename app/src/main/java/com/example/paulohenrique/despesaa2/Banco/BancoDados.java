package com.example.paulohenrique.despesaa2.Banco;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.paulohenrique.despesaa2.Tipos.Despesa;
import com.example.paulohenrique.despesaa2.Tipos.Usuario;

import java.util.ArrayList;

//importação para utilizar banco de dados sqlLite
public class BancoDados extends SQLiteOpenHelper {

    String[] scripCriaBanco = {"create table usuario (id integer primary key autoincrement, nome text not null, email text not null, senha text not null);"};

    String[] scriptDespesa = {"create table despesa (id integer primary key autoincrement, descricao text not null, data text not null, valor REAL not null);"};

    public final String apagaBancoSQL = "drop database if exists usuario";
    Context context;


    //Construto parammetrizado da classe
    public BancoDados(Context context, String name, int version) {
        super(context, name, null, version);
        this.context = context;
    }

    //Metodo para criar base de dados no banco sqlLite
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for (int i = 0; i < scripCriaBanco.length; i++) {
            sqLiteDatabase.execSQL(scripCriaBanco[i]);
        }

        for (int i = 0; i < scriptDespesa.length; i++) {
            sqLiteDatabase.execSQL(scriptDespesa[i]);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + scripCriaBanco);
        onCreate(sqLiteDatabase);

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + scriptDespesa);
        onCreate(sqLiteDatabase);
    }

    //Metodo para verificar se o usuário é valido no banco de dados
    public String validaUsuario(String email, String senha) {
        SQLiteDatabase bd = this.getReadableDatabase();
        String sql = "SELECT * FROM usuario WHERE email = ? AND senha = ?";
        Cursor c = bd.rawQuery(sql, new String[]{email, senha});
        if (c.getCount() > 0) {
            return "OK";
        }
        return "ERRO";
    }

    //metodo de inserção do usuario no banco de dados
    public void insereUsuario(ContentValues values) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.insert("usuario", null, values);

        database.close();

        Toast.makeText(context, "Usuário Cadastrado", Toast.LENGTH_SHORT).show();
    }

    //metodo de inserção da despesa no banco de dados
    public void insereDespesa(ContentValues values) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.insert("despesa", null, values);

        database.close();

        Toast.makeText(context, "Despesa cadastrada", Toast.LENGTH_SHORT).show();
    }

    //metodo que busca todas as receitas do banco de dados
    public ArrayList<Despesa> listaDespesa() {

        ArrayList<Despesa> despesaa = new ArrayList<>();

        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query("despesa", new String[]{"id", "descricao", "data", "valor"}, null, null, null, null, null);

        while (cursor.moveToNext()) {
            despesaa.add(new Despesa(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3)));
        }
        return despesaa;
    }

    //metodo que retorna todos os usuario do banco de dados
    //metodo nao está sendo utilizado
    public ArrayList<Usuario> listaUsuario() {

        ArrayList<Usuario> usuario = new ArrayList<>();

        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query("usuario", new String[]{"id", "nome", "email", "senha"}, null, null, null, null, null);

        while (cursor.moveToNext()) {
            usuario.add(new Usuario(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3)));
        }
        return usuario;
    }

    //metodo que soma todas as despesa inseridas no banco de dados
    public String somando() {
        Cursor cursor;
        String total = null;
        SQLiteDatabase database = this.getReadableDatabase();
        String sql = "SELECT SUM(valor) FROM despesa;";
        cursor = database.rawQuery(sql, null);

        if(cursor.moveToFirst()){
            total = cursor.getString(0);
            if(total != null) {
                return total;
            } else {
                Toast.makeText(context, "Nenhuma despesa cadastrada!", Toast.LENGTH_SHORT).show();
            }
        }
        return "R$00,00";
    }

    //metodo para atualizar as receitas cadastradas
    public void atualizar(Despesa despesa) throws Exception{
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("descricao", despesa.getDescricao());
        values.put("data", despesa.getData());
        values.put("valor ", despesa.getValor());
        database.update("despesa", values, "id = ? ", new String[]{"" + despesa.getId()});
    }

    //metodo de remoçao da receita
    public void deletaItem(Despesa despesa){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        database.delete("despesa", "id = ? ", new String[]{"" + despesa.getId()});
        database.close();
        Toast.makeText(context, "Despesa excluida!", Toast.LENGTH_SHORT).show();
    }
}