package com.example.paulohenrique.despesaa2.Controller;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.paulohenrique.despesaa2.Banco.BancoDados;
import com.example.paulohenrique.despesaa2.Mask;
import com.example.paulohenrique.despesaa2.R;

public class CadastrarDespesa extends AppCompatActivity {

    EditText editdescricao;
    EditText editdata;
    EditText editvalor;
    Button btcadastrarr;

    Intent intent = null;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_despesa);

        editdescricao = (EditText) findViewById(R.id.editdescricao);
        editdata = (EditText) findViewById(R.id.editdata);
        //responsavel por criar a mascara no xml
        editdata.addTextChangedListener(Mask.insert("##/##/####", editdata));
        editvalor = (EditText) findViewById(R.id.editvalor);
        btcadastrarr = (Button) findViewById(R.id.btcadastrarr);

        //ação botão cadastrar
        btcadastrarr.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //verifica se os campos estão vazios
                if (editdescricao.getText().length() == 0 || editdata.getText().length() == 0 || editvalor.getText().length() == 0) {
                    //caso estejam é apresentado uma mensagem solicitando o preenchimento
                    Toast.makeText(getApplicationContext(), "Preencha os campos corretamente", Toast.LENGTH_SHORT).show();
                } else {
                    //Contentvalues recebe os valores dos campos, cada valor possui uma chave de identificação
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("descricao", editdescricao.getText().toString());
                    contentValues.put("data", editdata.getText().toString());
                    contentValues.put("valor", editvalor.getText().toString());

                    //Instancia da classe Banco de Dados, onde passa a tela atual, nome da bd
                    BancoDados classBD = new BancoDados(getBaseContext(), "despesa", 1);
                    //passa para o metodo de inserir o conteudo do contentvalue
                    classBD.insereDespesa(contentValues);

                    //após ser feito a inserção é limpo os campos
                    editdata.setText("");
                    editdescricao.setText("");
                    editvalor.setText("");
                }
            }
        });
    }
}